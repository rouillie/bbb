#!/bin/bash                                                                                                    

if [ "$#" -ne 1 ]; then
    echo "Usage save_bbb.sh <PATH>"
    false
fi

save_path="$1"

if [ ! -d "$save_path" ]; then
    echo "Le répertoire $save_path n'existe pas"
    exit
fi


full_path=$(realpath "$0")
scriptdir=$(dirname "$full_path")

#rm -rf ${save_path}
if [ ! -d "${save_path}" ]; then
    mkdir "${save_path}"
fi


echo "Save letsnecrypt"
cd /etc
tar -zcf "${save_path}"/letsencrypt.tgz letsencrypt/archive letsencrypt/live letsencrypt/renewal
cd /root
echo "Save greenlight"
tar -zcf "${save_path}"/greenlight.tgz greenlight
cd

#customs default site identity and rooms
echo "Save customs"

cp -f /var/www/bigbluebutton-default/default.pdf "${save_path}"/.
cp -f /var/www/bigbluebutton-default/favicon.ico "${save_path}"/. 
cp -f /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties "${save_path}"/.
cp -f /usr/share/meteor/bundle/programs/server/assets/app/config/settings.yml "${save_path}"/.

FILE=/var/www/bigbluebutton-default/images/logo.png
if [ -f "$FILE" ]; then
    echo "Save logo"
    cp -f  /var/www/bigbluebutton-default/images/logo.png "${save_path}"/.
fi

#restore PHONE DIAL FILES (SIP)
FILE=/opt/freeswitch/conf/sip_profiles/external/phone_profile.xml
if [ -f "$FILE" ]; then
    echo "Save phone dial"
    cp -f /opt/freeswitch/conf/sip_profiles/external/phone_profile.xml "${save_path}"/.
    cp -f /opt/freeswitch/conf/dialplan/public/phone_plan.xml "${save_path}"/.
fi
