#!/bin/bash

new_ip=$(hostname -I | cut -f1 -d' ')

old_ip=$(awk '$1=="localIpAddress:" {print $2}' /usr/local/bigbluebutton/bbb-webrtc-sfu/config/default.yml)

if [ $new_ip != $old_ip ]; then
    FILE=/usr/share/red5/webapps/sip/WEB-INF/bigbluebutton-sip.properties
    if [ -f "$FILE" ]; then
	sed -i "s/$old_ip/$new_ip/g" /usr/share/red5/webapps/sip/WEB-INF/bigbluebutton-sip.properties
    fi;
    sed -i "s/$old_ip/$new_ip/g" /opt/freeswitch/etc/freeswitch/vars.xml
    sed -i "s/$old_ip/$new_ip/g" /usr/local/bigbluebutton/bbb-webrtc-sfu/config/default.yml						
    bbb-conf --clean
    #this last line could be replaced by : /opt/freeswitch/bin/fs_cli -p $(xmlstarlet sel -t -m 'configuration/settings/param[@name="password"]' -v @value /opt/freeswitch/etc/freeswitch/autoload_configs/event_socket.conf.xml) -x "reload mod_sofia"
    
fi;

