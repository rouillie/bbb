#!/bin/bash

if [ "$#" -lt 2 ]; then
    echo "Usage : install_bbb.sh <nom_du_sous_domaine> <adresse_mail>"
    echo "or"
    echo "install_bbb.sh <nom_du_sous_domaine> <adresse_mail> <turn param>"
    exit;
fi

sous_domaine="$1"
adresse_mail="$2"
# Optional
turn_param="$3"



apt-get update
apt-get install host

full_path=$(realpath "$0")
scriptdir=$(dirname "$full_path")

internal_ip=$(hostname -I | cut -f1 -d' ')
external_ip=$(curl http://ifconfig.me)
domain_ip=$(host "$sous_domaine" | awk '{print $4;exit;}')

echo "***********************************************************************";
if [ "$domain_ip" != "$external_ip" ]; then
    echo "IP du domaine ($domain_ip) et IP externe ($external_ip) différentes";
    echo "Vérifiez que le nom de domaine $sous_domaine est bien associé à l'adresse IP $external_ip";
    exit;
else
    echo "IP du domaine ($domain_ip) et IP externe ($external_ip) coincident";
fi
echo "***********************************************************************";


if [ -d /etc/letsencrypt/live/"${sous_domaine}" ]; then
   #certbot a ete lance
   if [ -f /opt/freeswitch/etc/freeswitch/vars.xml ]; then 
     #BBB a ete installe
     old_ext_ip=$(grep '<X-PRE-PROCESS cmd="set" data="external_rtp_ip=' /opt/freeswitch/etc/freeswitch/vars.xml | awk -F = '{print $4}' | awk -F \" '{print $1}')
     old_int_ip=$(awk '$1=="localIpAddress:" {print $2}' /usr/local/bigbluebutton/bbb-webrtc-sfu/config/default.yml)

     if [ "$old_ext_ip" != "$old_int_ip" ]; then
        #BBB a ete installe avec un IP externe differente de l'IP interne
	if [ "$old_ext_ip" != "$external_ip" ]; then
	   #affecte le domaine externe
	    bbb-conf --setip "$sous_domaine"
	    #corrige le fait que bbb-conf oublie un fichier
	    sed -i "s/$old_ext_ip/$external_ip/g" /opt/freeswitch/etc/freeswitch/vars.xml
	fi
     fi

     if [ "$internal_ip" != "$old_int_ip" ]; then
        #ip interna a change
	#corrige le fait que bbb-conf oublie des fichiers de config pour l'ip interne
        FILE=/usr/share/red5/webapps/sip/WEB-INF/bigbluebutton-sip.properties
        if [ -f "$FILE" ]; then
	    sed -i "s/$old_int_ip/$internal_ip/g" /usr/share/red5/webapps/sip/WEB-INF/bigbluebutton-sip.properties
        fi;
	sed -i "s/$old_int_ip/$internal_ip/g" /opt/freeswitch/etc/freeswitch/vars.xml
	sed -i "s/$old_int_ip/$internal_ip/g" /usr/local/bigbluebutton/bbb-webrtc-sfu/config/default.yml
     fi
   fi
fi

check_ubuntu(){
  RELEASE=$(lsb_release -r | sed 's/^[^0-9]*//g')
  if [ "$RELEASE" != $1 ]; then err "You must run this command on Ubuntu $1 server."; fi
}

check_ubuntu 18.04

if [ "$RELEASE" = "18.04" ]; then
    echo "Install 2.3";
    CMD_INST="bionic-24"
else 
   if [ "$RELEASE" = "16.04" ]; then
      echo "Install 2.2";
      CMD_INST="xenial-22"
   else
       echo "You are not running Ubuntu 16.04 or 18.04"
       exit;
   fi
fi

if [ "x$turn_param" == "x" ]; then 
	"${scriptdir}"/bbb-install-patched.sh -v $CMD_INST  -s "$sous_domaine" -e "$adresse_mail" -w -g
	cp -f /usr/share/bbb-web/WEB-INF/classes/spring/turn-stun-servers.xml /usr/share/bbb-web/WEB-INF/classes/spring/turn-stun-servers.xml.without_turn
else
	"${scriptdir}"/bbb-install-patched.sh -v $CMD_INST  -s "$sous_domaine" -e "$adresse_mail" -w -g -c "$turn_param"
	cp -f /usr/share/bbb-web/WEB-INF/classes/spring/turn-stun-servers.xml /usr/share/bbb-web/WEB-INF/classes/spring/turn-stun-servers.xml.with_turn
fi

if [ -d /var/lib/cloud/scripts/per-boot/ ]; then
    #on est sur un cloud avec cloud-init installe comme chez Scaleway
    cp ${scriptdir}/change_internal_ip.sh /var/lib/cloud/scripts/per-boot/.
fi;

docker exec greenlight-v2 bundle exec rake user:create["SuperDsi","superdsi@ouaibe.fr","superdsi","admin"]
