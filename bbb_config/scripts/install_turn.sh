#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage install_turn.sh <nom_du_sous_domaine> <adresse_mail>"
    exit;
fi

sous_domaine="$1"
adresse_mail="$2"

apt-get update
apt-get install host

full_path=$(realpath "$0")
scriptdir=$(dirname "$full_path")

internal_ip=$(hostname -I | cut -f1 -d' ')
external_ip=$(curl http://ifconfig.me)
domain_ip=$(host "$sous_domaine" | awk '{print $4;exit;}')


echo "***********************************************************************";
if [ "$domain_ip" != "$external_ip" ]; then
    echo "IP du domaine ($domain_ip) et IP externe ($external_ip) différentes";
    echo "Vérifiez que le nom de domaine $sous_domaine est bien associé à l'adresse IP $external_ip";
    exit;
else
    echo "IP du domaine ($domain_ip) et IP externe ($external_ip) coincident";
fi
echo "***********************************************************************";

clef_secrete=$(openssl rand -hex 16)

${scriptdir}/bbb-install-patched.sh  -c "$sous_domaine:$clef_secrete" -e "$adresse_mail"
echo "******************************************************************************************************************";
echo "The last parameter to send to install_bbb.sh in order to configure your BBB server : $sous_domaine:$clef_secrete"
echo "******************************************************************************************************************";

