#!/bin/bash

# Teste que le script est lance en root
if [ $EUID -ne 0 ]; then
  echo "Le script doit etre lance en root"
  exit;
fi

if [ "$#" -ne 2 ]; then
    echo "Usage: bbb_monitoring <nom_du_sous_domaine> <passwd pour accéder à sous-domaine/metrics>"
    exit;
fi

sous_domaine=$1
pwd=$2

apt-get update
apt-get install host

internal_ip=$(hostname -I | cut -f1 -d' ')
external_ip=$(curl http://ifconfig.me)
domain_ip=$(host $sous_domaine | awk '{print $4;exit;}')


echo "***********************************************************************";
if [ "$domain_ip" != "$external_ip" ]; then
    echo "IP du domaine ("$domain_ip") et IP externe ("$external_ip") différentes";
    echo "Vérifiez que le nom de domaine "$sous_domaine" est bien associé à l'adresse IP "$external_ip;
    exit;
else
    echo "IP du domaine ("$domain_ip") et IP externe ("$external_ip") coincident";
fi
echo "***********************************************************************";


if [ -d "$HOME/bbb-monitoring" ]; then
	read -p "Le répertoire $HOME/bbb-monitoring existe deja (probablement créé par ce script et dans ce cas c'est ok de continuer). On continue [O]:" answerdir
	answerdir=${answerdir:-O}
	if [ "$answerdir" != "O" ]; then
		echo "Effectivement... autant faire une sauvegarde..."
		exit;
	fi
	else
		mkdir $HOME/bbb-monitoring
fi

if [ -f /etc/bigbluebutton/nginx/monitoring.nginx ]; then
	read -p "Le fichier /etc/bigbluebutton/nginx/monitoring.nginx existe deja (probablement créé par ce script et dans ce cas c'est ok de continuer). On continue [O]:" answernginx
	answernginx=${answernginx:-O}
	if [ "$answernginx" != "O" ]; then
		echo "Effectivement... autant faire une sauvegarde..."
		exit;
	fi
fi

cd $HOME/bbb-monitoring/

rm $HOME/bbb-monitoring/bbb_exporter_secrets.env $HOME/bbb-monitoring/docker-compose.yaml $HOME/bbb-monitoring/prometheus.yaml

wget -c https://gitlab.inria.fr/rouillie/bbb/-/raw/master/bbb_config/monitoring/bbb_exporter_secrets.env
wget -c https://gitlab.inria.fr/rouillie/bbb/-/raw/master/bbb_config/monitoring/docker-compose.yaml
wget -c https://gitlab.inria.fr/rouillie/bbb/-/raw/master/bbb_config/monitoring/prometheus.yaml

apiurl=$(bbb-conf --secret | awk '{print $2}' | head -2 | tail -1)"api/"
secret=$(bbb-conf --secret | awk '{print $2}' | head -3 | tail -1)

sed -i "s|https://example.com/bigbluebutton/api/|$apiurl|g" $HOME/bbb-monitoring/bbb_exporter_secrets.env 
sed -i "s|[<]SECRET>|$secret|g" $HOME/bbb-monitoring/bbb_exporter_secrets.env 
sed -i "s|example.com|$sous_domaine|g" $HOME/bbb-monitoring/docker-compose.yaml

docker-compose up -d

apt install -y apache2-utils

if [ -f /etc/nginx/.htpasswd ]; then
	htpasswd -b /etc/nginx/.htpasswd metrics $2
else
	htpasswd -c -b /etc/nginx/.htpasswd metrics $2
fi

rm monitoring.nginx
wget -c https://gitlab.inria.fr/rouillie/bbb/-/raw/master/bbb_config/monitoring/monitoring.nginx
cat monitoring.nginx > /etc/bigbluebutton/nginx/monitoring.nginx

nginx -t

if [ $? != 0 ]; then
	echo "Quelque chose s'est mal passé avec nginx, tapez 'nginx -t' pour en savoir plus..."
	exit;
fi

systemctl reload nginx


