# A collection of scripts for a flexible and easy installation, configuration and use of BigBlueButton
[BigBlueButton](https://bigbluebutton.org/) is an extremely powerful open-source video conferencing software that is easy to install in a number of cases. It is not natively designed for the host server to be on a home network. Essentially it does not support changing IP addresses or fickle firewalls.

We have developed a series of scripts that complement the official script in order to propose an easy and flexible installation.

A single command whatever the situation (physical or virtual machine, with or without a firewall, behind a box or not, etc.):

    install_bbb.sh <nom_de_machine> <adresse_mail>

You have the choice to :
 
- download a virtual machine containing a complete installation: it will only take a few minutes to configure it and install it on your network (one to 2 minutes as long as your firewall is correctly configured)
- install an Ubuntu 16.04 (for BBB 2.2.XX)   or Ubuntu 18.04 (for BBB 2.3.XX) linux distribution yourself, the script will then install everything you need and configure your BigBlueButton

The scripts will also allow you to move your installation from one machine to another without losing any data relating to your video.

## Prerequisites

* A machine with
    * at least
        * a processor with at least 4 threads available (so a machine with 6 or 8 threads if you want to use a virtual machine)
        * 8 GB of RAM and 20 GB of disk available (so 16 GB of RAM and 40 GB of disk if you want to use a virtual machine)
    * ideally
        * a processor with at least 8 threads available (no need to have more than 16 threads)
        * 16 GB of RAM (no need to have more than 32 GB) and 20 GB of disk available 
* A subdomain name that points to the EXTERNAL IP of the machine.
* If you are behind a NAT or an internet Box :
    * Make sure that the subdomain name points to the IP of the firewall or the box.
    * Make sure that it will be possible to redirect the following ports to the machine (virtual or not) when you know its local IP:
        * TCP 80, 443 and 1935
        * UDP from 16384 to 32768

## Prepare your host
You can 

- [download and use our virtual machine ***SuperDsi***](docs/virtual_machine.md)

Or 

- use any machine running Ubuntu 16.04 (the script will then install BBB 2.2.XX) or 18.04 (the script will install BBB 2.3.XX). You can, for example, set a [Sacelway Instance](docs/prepare_scaleway.md)

    You will then need to clone our directory` git https://gitlab.inria.fr/rouillie/bbb` in the `/root` directory :

        sudo -i
        cd /root
        apt-get update
        apt-get install -y git
        git clone https://gitlab.inria.fr/rouillie/bbb

## Prepare your firewall (or your internet box) 
* If not already done, redirect the following ports of the firewall (or internet box) to the machine (virtual or not):
    * TCP 80, 443 and 1935
    * UDP from 16384 to 32768
See how to do this operation for the French internet providers [Free](docs/freebox.md) and [Bouygues](docs/bbox.md)

## Install and configure the BigBlueButton server
Start your machine (virtual or not).

Whatever your basic medium (virtual machine or physical machine, firewall or not, etc.), connect to an account allowing access to super-user rights, and then type

    sudo -i
    /root/bbb/bbb_config/scripts/install_bbb.sh <name_of_subdomain> <mail_address>

## Test your installation
If all went well, the script has installed the videoconference which is now usable at the address `http://<name_of_the_sub_domain>`

You can login using the identifier `superdsi@ouaibe.fr` and the password` superdsi`.

> ** IMPORTANT ** You must immediately create an administrator account then destroy the `superdsi@ouaibe.fr` account in the` greenlight` interface

## Make your installation resistant to a change of internal IP
A change of local IP makes the installation unusable. This is what usually happens when the machine is connected using DHCP on a personal router or often when turning off/on a virtual instance in the cloud. Each time it happens, you can correct the problem by launching the following command :

    /root/bbb/bbb_config/scripts/change_internal_ip.sh

There are 2 possibilities to correct this problem permanently

- assign a static IP to the machine if you can access to the configuration of the router
- on Scaleway (and probably almost all public clouds), make use of cloud-init to execute `/root/bbb/bbb_config/scripts/change_internal_ip.sh`at boot time. On Scaleway cloud, just copy this file in the directory `/var/lib/cloud/scripts/per-boot/`

    cp /root/bbb/bbb_config/scripts/change_internal_ip.sh /var/lib/cloud/scripts/per-boot/.

## Update your installation (to be done regularly)

First update the scripts :

    cd /root/bbb
    sudo git pull

Then launch the installation script as for a fresh install 

    sudo -i
    /root/bbb/bbb_config/scripts/install_bbb.sh <name_of_subdomain> <mail_address>

## [optional] Optimize your server

### Kurento (the multimedia server used by BBB)

Force the network interface to be used : set `networkInterfaces` properly in `/etc/kurento/modules/kurento/WebRtcEndpoint.conf.ini`

Use 3 servers in parallel : add `enableMultipleKurentos`on a single line in the file `/etc/bigbluebutton/bbb-conf/apply-config.sh`

There are many parameters that can be set in the (documented) file /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties. Note that you might preferably override these parameters using the file /etc/bigbluebutton/bbb-web.properties.


