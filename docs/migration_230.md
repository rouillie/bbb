# Preparation

## Save configuration and accounts

    mkdir /root/save
    ./bbb_config/scripts/save_bbb.sh /root/save/visio.animath.live

All has been saved in the directory /root/save

## Create an Ubuntu 18.04 instance

## [OPTIONAL] One trick that might avoid disk saturation 

- 20 Go for the system (in fact 15 are sufficient)
- 20 Go for recordings and logs (1 might be sufficient if you disable all possible recordings)

## [OPTIONAL] Reuse your current greenlight configuration

Just put the old greenlight directory in /root/.

If you did save your configuration using the above script you just need to uncompress the file greenlight.tgz in /root

# Installation

Just type the following lines : 

    apt update
    cd
    git clone https://gitlab.inria.fr/rouillie/bbb
    ./bbb/bbb_config/scripts/install_bbb_beta.sh <SUBDOMAIN_NAME> <VALID_EMAIL_ADDRESS>

For Scaleway users, make your installation resistant to reboot in case of a change of local IP (DHCP for example) by copying the file `/root/bbb/bbb_config/scripts/change_internal_ip.sh no_restart` in the directory `/var/lib/cloud/scripts/per-boot/`:

    cp /root/bbb/bbb_config/scripts/change_internal_ip.sh /var/lib/cloud/scripts/per-boot/.

## [OPTIONAL] Migrate greenlight

https://docs.bigbluebutton.org/greenlight/gl-config.html#upgrading-postgresql-versions

**Remark** : do not pay too much attention to the error `greenlight_production does not exist` it seems that is not correct. The right way to check is to test.

## [OPTIONAL] Mount separate directories for recordings and logs

Stop Bigbluebutton

    bbb-conf -stop
    
Get the second disk you can access if any :

    fdisk -l

Format it. For example if it is the device `/dev/vdb` :

    mkfs.ext4 /dev/vdb

create a ddirectory to mount it :

    mkdir /mnt/bbb_data

mount it :

    mount /dev/vdb /mnt/bbb_data
    
move /var/bigbluebutton  /var/freeswitch /var/lib/kurento /var/log to /mnt/bbb_data and put simlinks instead

    mv /var/bigbluebutton /mnt/bbb_data/.
    ln -s /mnt/bbb_data/bigbluebutton /var/bigbluebutton
    mv /var/freeswitch /mnt/bbb_data/.
    ln -s /mnt/bbb_data/freeswitch /var/freeswitch
    mv /var/lib/kurento /mnt/bbb_data/.
    ln -s /mnt/bbb_data/kurento /var/lib/kurento
    mv /var/log /mnt/bbb_data/.
    ln -s /mnt/bbb_data/log /var/log

Change the rights for /var/bigbluebutton

    sudo chown -R bigbluebutton:bigbluebutton /var/bigbluebutton

make your mount persistant by modifying the file /etc/fstab adding the following line 

    /dev/vdb /mnt/bbb_data ext4 defaults 0 0

Restart Bigbluebutton

    bbb-conf -start
    
