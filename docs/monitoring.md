# Add monitoring to your BBB server with Prometheus and Grafana

The Grafana/Prometheus couple allows good monitoring features where Prometheus is a Time Series data management system and Grafana a visualising software with dashboards.

This installation is inspired from https://bigbluebutton-exporter.greenstatic.dev/ and the bbb_monitoring.sh script does automatically what is described in this documentation.

# Installation of a monitoring Web site on your BBB server

First, you need a BBB server installed. Preferably with the scripts available in this git repository. We assume that you already cloned this git as described in the BBB install documentation.

Go to the scripts directory and launch the script, it should look like:
	
	cd ~/bbb/bbb_config/scripts/
	./bbb_monitoring.sh <NAME_OF_BBB_SERVER> <PWD>

Where NAME_OF_BBB_SERVER is the subdomain of your server (e.g. visio.animath.live) and PWD is the password prometheus will use to access the metrics (which will be available at "/metrics", like visio.animath.live/metrics for instance).

If the script finds existing directories or files that will be overwritten then it will ask you to confirm (juste type enter for "O", the default answer, and it will go ahead, otherwise type anything else and it will stop, allowing you to back these files up if you want).

# After the installation, how to use Grafana/Prometheus

Login to NAME_OF_BBB_SERVER/monitoring (visio.animath.live/monitoring in our example) where user and password will be admin:admin (this is the default Grafana settings). You will be asked to modify the password immediately.

**Add Prometheus as data source**

Once in Grafana, add a data source. First, click on the gear icon on the left => Data Sources

![data_source](images/data_source.png)

Then click on "Add data source", select "Prometheus" and type http://localhost:9090 in the URL field (pay attention, you can read "http://localhost:9090" in the field but it is only a placeholder, you really need to type it in the field).
Check the "Basic auth" box, enter user (metrics) and password (PWD, the one you gave to the script) click "SAVE AND TEST" and it should read "Data source is working".

![data_source_working](images/data_source_working.png)

**Import a dashboard**

Now click the "+" sign in the left menu and "Import" for importing a new dashboard.
There you have two options.

Option 1: copy-paste the content of the json model given at https://raw.githubusercontent.com/greenstatic/bigbluebutton-exporter/master/extras/dashboards/server_instance_node_exporter.json
Click load and your dashboard will display. Just one last step: your dashboard will probably display "No data". Just select a datasource in the upper left corner, you can choose between "Prometheus" and "default". Select Default.

Option 2: copy-paste the content of the json model given at https://gitlab.inria.fr/rouillie/bbb/-/raw/master/bbb_config/monitoring/dashboard_with_alerts.json

This option includes alerts which is neat but needs a bit more configuration. 

**Managing alerts (only for option 2)**

Data is scrapped and updated in Grafana every 10 seconds, which is then the minimum delay for an alert to be triggered. Default values for these alerts are:
- CPU>70%
- Total number of participants > 150
- Total number of webcams > 40
- Transmit > 1Gbps
- Total number of rooms > 20

The alert panels can be seen at the very bottom of this dashboard and are well identified, with "/!\\" in the title. 
The "Transmit" alert panel (bottom of the page) may need an update because the network interface is specified directly. This is because Grafana does not allow alerts with variable:
- Click "edit" on the "/!\ For Transmit alerts only /!\\" panel. 
- Ihe Metrics field: "irate(node_network_transmit_bytes_total{job="bbb_node_exporter", instance="localhost", device="ens2"}[15s])*8" replace "ens2" in with the actual interface (the list of available network interfaces for your environment is at the top of the page).

Globally, to modify an alert: click on the scrolling menu in the title of the panel dedicated to that alert => edit => alert tab and modify the "is above" field.

You also need to configure an email server for sending alerts. Your server must have ssmtp installed (on Scaleway this is an option that has to be asked explicitely even if it is not paying) and configured. This is an example of values for a gmail account:

![ssmtp](images/ssmtp.png)

On Scaleway, with Ubuntu Xenial, the file is /etc/ssmtp/ssmtp.conf.

Check that your smtp account is okay by
	
	echo "E-Mail using the command-line" | ssmtp your.name@your_emailprovider.ext
	
You should receive an email with that echo text in the specified Inbox.

Then add a notification channel (left menu => the "bell" icon => notification channels) where the fields are rather explicit.

Add the smtp information in the grafana initialization by docker. Update the "~/bbb-monitoring/docker-compose.yaml" file and in the grafana "environment" section add:
- GF_SMTP_ENABLED: "true"
- GF_SMTP_HOST: "your smtp info"
- GF_SMTP_USER: "the email account for sending alerts"
- GF_SMTP_PASSWORD: "pwd of the email account"


**Save your dashboard**

Go to the gear icon in the upper right corner of the current dashboard that you want to save

![dashboardsettings](images/dashboardsettings.png)

Then go to the Json model tab on the left

![jsonmodel](images/jsonmodel.png)

You can copy the json content of the dashboard and import it as another dashboard, or use the "save as" button and keep a version of it in Grafana.
