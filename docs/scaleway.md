# How to install your own BigBlueButton instance on a Scaleway server in 15 minutes

This page describes how to install your own instance of [BigBlueButton](https://bigbluebutton.org/) on a private server hosted by [Scaleway](https://www.scaleway.com/en/). The procedure is extremely simple and takes about 15 minutes.

Having your own server dedicated to video conferencing can be quite costly (around 100€/month). But with the solution we propose here, after the installation phase, you can turn the server on and off with a one line instruction. Hence, you will only pay the server costs when you need to use it! Therefore, this will only cost you about 0.20€/hour of use. 

## Prerequisite

- Have an account at [Scaleway](https://www.scaleway.com/en/). If you don't have one, it takes a few minutes to create one. Don't forget to upload your public key on your Scaleway account.
- Install the [Command Line Interface](https://github.com/scaleway/scaleway-cli/) of Scaleway on your computer and initiate it. If you don't have it yet, it takes 5 minutes to install. Then you will be able to administer all your servers in command line!
- Own a (sub)domain name for which you can change the DNS. Any registrar will do. In my case I use OVH.

## Create a new instance at Scaleway

Log in to your Scaleway account in the [Scaleway Console](https://console.scaleway.com/). In the top menu, click on "Create" > "Instance". Then follow the steps.

1. Availability zone. Choose your preferred availability zone.
2. Image. You need to install an image of Ubuntu 16.04, aka Ubuntu Xenial or of Ubuntu 18.04, aka Ubuntu Bionic. Be careful, BBB only runs on these versions of Ubuntu!
3. Instance size. Depends on the intended usage. In my case I use GP1-S.
4. Volumes. Keep the default options. No additional volume required.
5. Name. Choose a name for your new machine.

Wait a few seconds, there you go, you have your new server.
Note that you could have done this using the Command Line Interface but for the creation let's keep it simple.

Now you can see all the information about your new server. There are two important things that you will need later, so keep them handy.
- the instance ID: should look like "8c5ea9b7-3c42-40a1-bf57-34e91fd09ec8"
- the public IP address: something like "51.123.45.678"

## Redirecting the domain name to the new server

Go to your domain name registrar administration panel. Chose the (sub)domain name that you want to dedicate to this new server. For instance bbb.mydomain.com. Modify the DNS so that this subdomain points to the public IP of your server.

## Installing BigBlueButton (who would have thought it would be that easy)

Log in to your machine using the new address.
```
ssh root@bbb.mydomain.com
```

Install some basic software.
```
sudo apt update
sudo apt install unzip
sudo apt install host
sudo apt install git
```

Download our (BigBlueButton installation script)[https://gitlab.inria.fr/rouillie/bbb], unzip it and go to the scripts directory.
```
cd /root
git clone https://gitlab.inria.fr/rouillie/bbb
cd bbb/bbb_config/scripts
```

Now it's only a one line instruction to launch the installation.
```
./install_bbb.sh bbb.mydomain.com myemailaddress@mail.com
```

The installation should take about 5 minutes. 
 

That's it, you can now exit your server.
```
exit
```

That's it! Your BBB server is up and running. Go to bbb.mydomain.com to try it. 
You can log in with
- email: superdsi@ouaibe.fr
- password: superdsi

This gives you access to the admin account. From there you can change the mail and password, create additional accounts, change parameters etc.

## Turning on and off the server (where the magic happens)

Now from your computer where you have installed the Scaleway Command Line Interface, you can turn off the server. All you need is the instance ID (long hexadecimal number).

- To stop the server, type the following instruction.
```
scw instance server stop MYINSTANCEID
```

Now you can check that bbb.mydomain.com will not respond anymore.

- The following instruction will start the server again. 
```
scw instance server start MYINSTANCEID
```

Wait a few minutes and go to bbb.mydomain.com. The site is back on, just like you left it! 

Note that turning off the server takes about 1 minute, turning it back on should take 2 to 3 minutes. To check on the status of your server, use the following command.
```
scw instance server list
```

Tip: create one line scripts `bbb_start.sh` and `bbb_stop.sh` so that you can turn your server on and off easily.

## How much does that cost?

The server (GP1-S) costs 0.17€/hour (plus 20% taxes, around 0.20€/hour). But you also need to pay to keep the public IP dedicated to your server (otherwise the public IP would change and you would need to reconfigure the DNS each time). This brings an additional fix cost of 1.00€/month.

One more thing, don't forger to turn off your server when you are done. Scaleway has an option to send you an alert when your estimated billing exceeds some amount, this can be useful. You can also configure a cron to turn off the server everyday during the night.

Finally, if you ever need to host a bigger virtual event (for instance with a hundred webcams at the same time) you can create a disk image of your server, then create a new (bigger) instance at Scaleway and load it with your disk image. All you need to do then is to detach the public IP from you (small) server and attach it to the new (bigger) one. Easy! When you are done, attach the public IP back to the small server and delete the big one. Hence you can host big events for a few euros a day!
