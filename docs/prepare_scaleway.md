This page describes how to create your own instance for installing [BigBlueButton](https://bigbluebutton.org/) on a private server hosted by [Scaleway](https://www.scaleway.com/en/). The entire procedure is extremely simple and takes about 15 minutes.

Having your own server dedicated to video conferencing can be quite costly (around 100€/month). But with the solution we propose here, after the installation phase, you can turn the server on and off with a one line instruction. Hence, you will only pay the server costs when you need to use it! Therefore, this will only cost you about 0.20€/hour of use. 

## Prerequisite

- Have an account at [Scaleway](https://www.scaleway.com/en/). If you don't have one, it takes a few minutes to create one. Don't forget to upload your public key on your Scaleway account.
- Install the [Command Line Interface](https://github.com/scaleway/scaleway-cli/) of Scaleway on your computer and initiate it. If you don't have it yet, it takes 5 minutes to install. Then you will be able to administer all your servers in command line!
- Own a (sub)domain name for which you can change the DNS. Any registrar will do. In my case I use OVH.

## Create a new instance at Scaleway

Log in to your Scaleway account in the [Scaleway Console](https://console.scaleway.com/). In the top menu, click on "Create" > "Instance". Then follow the steps.

1. Availability zone. Choose your preferred availability zone.
2. Image. You need to install an image of Ubuntu 16.04, aka Ubuntu Xenial. Be careful, BBB only runs on this version of Ubuntu!
3. Instance size. Depends on the intended usage. In my case I use GP1-S.
4. Volumes. Keep the default options. No additional volume required.
5. Name. Choose a name for your new machine.

Wait a few seconds, there you go, you have your new server.
Note that you could have done this using the Command Line Interface but for the creation let's keep it simple.

Now you can see all the information about your new server. There are two important things that you will need later, so keep them handy.
- the instance ID: should look like "8c5ea9b7-3c42-40a1-bf57-34e91fd09ec8"
- the public IP address: something like "51.123.45.678"

## Redirecting the domain name to the new server

Go to your domain name registrar administration panel. Chose the (sub)domain name that you want to dedicate to this new server. For instance bbb.mydomain.com. Modify the DNS so that this subdomain points to the public IP of your server.

## Installing BigBlueButton (who would have thought it would be that easy)

Log in to your machine using the new address.
```
ssh root@bbb.mydomain.com
```

Install some basic software.
```
sudo apt update
sudo apt install git
```
