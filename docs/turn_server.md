# Set a turn serveur and configure BigBlueButton accordingly

BBB opens a lot of ports different ports, which might prevent users behind a restrictive firewall to use it.
A solution consists in using an intermediate server, named turn server for using only the usual 80 (http) and 443 (https) ports.
**The turn server must be on a different machine than the BBB server** and thus it might first be installed separately and then the BBB server must be specifically configured.

## Installation of a turn server 

First ensure that you properly declare the name of your turn server in your DNS zone.

**On a server running Ubuntu 20.04**, install the scripts like for the installation of the BigBlueButton server

    sudo -i
    apt-get update
    apt-get install -y git
    git clone https://gitlab.inria.fr/rouillie/bbb

Then, launch the installation of the server itself

    /root/bbb/bbb_config/scripts/install_turn.sh < THE_NAME_OF_THE_TURN_SERVER >  < A_MAIL_ADDRESS >

At the end of the installation process, a parameter for the configuration of BigBlueButton will be printed, paste and copy this information.

For example, if you launch 

>/root/bbb/bbb_config/scripts/install_turn.sh turn.animath.live fabrice.rouillier@animath.fr

Something like :

>******************************************************************************************************************
>The last parameter to send to install_bbb.sh in order to configure your BBB server : turn.animath.live:85c05cf46e9d799cce959b611c8098f4
>******************************************************************************************************************

## Configuration of the BBB server 
On the BigBlueButton server, just launch the install_bbb.sh script with the configuration information given by the installation process on the turn server as a third parameter.

    /root/bbb/bbb_config/scripts/install_bbb.sh < THE_NAME_OF_THE_BBB_SERVER >  < A_MAIL_ADDRESS >  < CONFIGURATION_INFORMATION_FROM_TURN >

For example, following the above example, you might lauch 

>/root/bbb/bbb_config/scripts/install_bbb.sh visio.animath.live fabrice.rouillier@animath.fr turn.animath.live:85c05cf46e9d799cce959b611c8098f4


> **Remark :** if you lost the configuration information from the turn server, it has the following shape
> \< SERVER_NAME \>:\< SECRET_KEY\> and both informations can be found in the file  \/etc\/turnserver.conf

