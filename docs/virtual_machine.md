# The virtual machine ***SuperDsi***
You have the possibility to download a virtual machine with BigBlueButton as well as all the pre-installed scripts

## Download
Download the file (4.5 Go) https://www.animath.live/Downloads/BBB-Perso.ova

Then import the virtual machine into the virtualization software of your choice (VirtualBox for example).

## Memory settings
Increase the size of memory allocated to the virtual machine to at least 8192 MB (it is 4096 MB by default for compatibility reasons). It is recommended to allocate 16MB.

## Network card configuration
Choose the network card of the host machine that is connected to your local network and switch to bridge mode.
Note the mac address (or define one) you will need it to configure your firewall.

## Login as a superuser and update the scripts
The administrator login  is `superdsi` with the password` superdsi`.

> ** IMPORTANT ** change the password of `superdsi` ** IMMEDIATELY ** (command` passwd`) or, better ... create a superuser account then ** transfer SuperDSI ** (`userdel -r superdsi `).

Update the scripts :

    sudo -i
    cd /root/bbb
    git pull
