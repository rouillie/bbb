# Utilisateurs Livebox 5

Les indications suivantes ne connernent que la Livebox 5 d'Orange.

Si votre serveur est derrière la box, il faut s'assurer que le nom de votre sous-domaine pointe sur l'IP public de la box.
Pour vous connecter à l'interface d'administration de votre Livebox, tapez <https://livebox/> dans votre navigateur en étant 
connecté au résseau local.  

L'adresse IP publique de la Livebox est donnée dans **Informations système/Internet** accessible via la page **Paramètres système** suivante.
**Attention :** l'adresse IP publique de la Livebox change après chaque re-initialisation.

![livebox-parametres-systeme](images/livebox-parametres-systeme.png)

## Ajout d'une adresse IP statique

Revenir sur la page  **Paramètres système** et cliquer sur l'icone **Réseau**.  

![livebox-select-statique](images/livebox-select-statique.png)

Sur la nouvelle page, dans la sous-section "**Baux statiques**", cliquez sur l'équipement  à rajouter en statique (e.g., visio dans l'exemple). Pour 
valider cliquer sur **Ajouter**. Le résultat est le suivant:
 
![livebox-statique-valide](images/livebox-statique-valide.png)

## Redirection de ports via DMZ

Il suffit de cliquer sur l'onglet **DMZ** et puis de selection l'équipement vers lequel pointer (e.g., visio dans l'exemple).
N'oubliez pas de cliquer sur **Enregistrer** pour valider.

![livebox-dmz-valide](images/livebox-dmz-valide.png)

## Remarques très importantes 

1. Il est toujours conseillé de retirer l'adresse IP statique avant d'arrêter la machine virtuelle. Si vous ne le faites pas, la Livebox attribuera une mauvaise adresse MAC à l'IP statique lors du prochain lancement de la machine virtuelle. 

2. La première conséquence du point précént est qu'il faut s'assurer que l'installation BBB dans la machine virtuelle est robuste para rapport 
à des changents d'IP. Pour cela il faut absolument rajouter la ligne /root/bbb/bbb_config/scripts/change_internal_ip.sh juste avant exit 0 dans le fichier /etc/rc.local

3. Si vous oubliez le point 1, il suffit de renouveler l'adresse MAC de la machine virtuelle dans la page **Settings/Network** de VirtualBox.

![livebox-virtualbox-network](images/virtualbox-network.png)
