## Migrate an installation
The objective is to recover the accounts but also the complete configuration (except records) to change the machine.

### On the source machine
Log in then switch to super-user with the command

    sudo -i

Start the backup by specifying an email address

    /root/bbb/bbb_config/scripts/save_bbb.sh <a directory>

this will store all the files needed for account management and configuration, including SSL certificates in `<a directory>`

### On the target machine

It is assumed that the target machine is ready to receive the installation of the video (or is already fully installed with the video)

Log in then switch to super-user with the command

    sudo -i

Recover the backup directory and copy it to `<a directory>`

Start the reconfiguration script

    root/bbb/bbb_config/scripts/restore_bbb.sh <a directory>
